const express = require("express");
const webRoute = express.Router();
const { userGames, usergameBiodata, usergameHistory } = require("./models");

webRoute.get("/home", (req, res) => {
  res.render("home");
});

webRoute.get("/home/user", async (req, res) => {
  const data = await userGames.findAll();

  res.render("user", {
    data,
  });
});

webRoute.get("/home/user/biodata", async (req, res) => {
  res.render("biodata");
});

module.exports = webRoute;
