"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class usergameHistory extends Model {
    static associate(models) {
      const { userGames } = models;
      usergameHistory.belongsTo(userGames, {
        foreignKey: "userGames",
      });
    }
    static associate(models) {
      // define association here
    }
  }
  usergameHistory.init(
    {
      job: DataTypes.STRING,
      player: DataTypes.STRING,
      result: DataTypes.STRING,
      createdAt: DataTypes.DATE,
      updatedAt: DataTypes.DATE,
    },
    {
      sequelize,
      modelName: "usergameHistory",
    }
  );
  return usergameHistory;
};
