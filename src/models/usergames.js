"use strict";
const { Model } = require("sequelize");
const usergamebiodata = require("./usergamebiodata");
module.exports = (sequelize, DataTypes) => {
  class userGames extends Model {
    static associate(models) {
      userGames.belongsTo(models.userGames, {
        foreignKey: "userGames",
      });
    }
  }
  userGames.init(
    {
      email: DataTypes.STRING,
      username: DataTypes.STRING,
      password: DataTypes.STRING,
      createdAt: DataTypes.DATE,
      updatedAt: DataTypes.DATE,
    },
    {
      sequelize,
      modelName: "userGames",
    }
  );
  return userGames;
};
