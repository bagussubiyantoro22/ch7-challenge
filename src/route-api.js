const express = require("express");
const route = express.Router();
const { User } = require("./models");
const passport = require("passport");
const jwt = require("jsonwebtoken");
const { Strategy: JWTStrategy, ExtractJwt } = require("passport-jwt");

passport.use(
  new JWTStrategy(
    {
      jwtFromRequest: ExtractJwt.fromHeader("authorization"),
      secretOrKey: "secret",
    },
    async (payload, done) => {
      try {
        const user = await User.findByPk(payload.id);

        return done(null, user);
      } catch (error) {
        return done(null, false, { message: error.message });
      }
    }
  )
);
route.get("/", (req, res) => {
  res.json({ success: "ok" });
});

route.post("/login", async (req, res, next) => {
  try {
    const user = await User.authenticate(req.body);
    const today = new Date();
    const accessTokenExpiredAt = today.setHours(today.getHours() + 2);
    res.json({
      accessToken: jwt.sign(
        {
          id: user.id,
          username: user.username,
          accessTokenExpiredAt,
        },
        "secret"
      ),
      accessTokenExpiredAt,
    });
  } catch (error) {
    next(error);
  }
});

route.get(
  "/user",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    res.json(req.user);
  }
);

module.exports = route;
