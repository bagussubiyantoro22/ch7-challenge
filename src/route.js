const express = require("express");
const passport = require("passport");
const route = express.Router();
const LocalStrategy = require("passport-local");
const { isAuthenticated } = require("./utils/middleware");
const { userGames, usergameBiodata, usergameHistory } = require("./models");
const { User } = require("./models");

route.get("/", (req, res) => {
  res.render("index");
});

route.get("/register", (req, res) => {
  res.render("register", { status: 200 });
});

route.post("/register", async (req, res) => {
  const { name, username, password } = req.body;

  if (!name || !username || !password) {
    res.render("register", {
      status: 500,
    });
  } else {
    await User.register({ name, username, password });

    res.redirect("login");
  }
});

route.get("/login", (req, res) => {
  if (req.isAuthenticated()) return res.redirect("/user");

  res.render("login");
});

passport.use(
  new LocalStrategy(async (username, password, done) => {
    try {
      const user = await User.authenticate({ username, password });

      return done(null, user);
    } catch (error) {
      return done(null, false, { message: error.message });
    }
  })
);

passport.serializeUser(async (user, done) => {
  const { id, username } = await user;
  done(null, { id, username });
});

passport.deserializeUser(async (user, done) => {
  done(null, await user);
});

route.post(
  "/login",
  passport.authenticate("local", {
    successReturnToOrRedirect: "/user",
    failureRedirect: "/login",
    failureMessage: true,
  })
);

route.get("/user", isAuthenticated, (req, res) => {
  console.log(req.user);
  res.render("user", { username: req.user.username });
});

route.get("/logout", (req, res, next) => {
  req.logout((err) => {
    if (err) return next(err);

    res.redirect("/");
  });
});

// ============== Create user  ==============

// route.post("/user", async (req, res) => {
//   const { body } = req;
//   const createuserOK = await userGames.create(body);

//   res.json(createuserOK);
// });

// route.get("/user", async (req, res) => {
//   const pageuserOK = await userGames.findAll();
//   res.json(pageuserOK);
// });

// route.put("/user/:id", async (req, res) => {
//   const { body } = req;
//   const u_userok = await userGames.update(body, {
//     where: {
//       id: req.params.id,
//     },
//   });
//   res.json(u_userok);
// });

// route.delete("/user/:id", async (req, res) => {
//   const d_userok = await userGames.destroy({
//     where: {
//       id: req.params.id,
//     },
//   });
//   res.json(d_userok);
// });

// ========== Create usergameBiodata ==============

route.post("/userbiodata", async (req, res) => {
  const { body } = req;
  const createbiodataOK = await usergameBiodata.create(body);
  res.json(createbiodataOK);
});

route.get("/userbiodata", async (req, res) => {
  const createbiodataOK = await usergameBiodata.findAll();
  res.json(createbiodataOK);
});

route.put("/userbiodata/:id", async (req, res) => {
  const { body } = req;
  const u_userbiodataok = await usergameBiodata.update(body, {
    where: {
      id: req.params.id,
    },
  });
  res.json(u_userbiodataok);
});

route.delete("/userbiodata/:id", async (req, res) => {
  const u_userbiodataok = await usergameBiodata.destroy({
    where: {
      id: req.params.id,
    },
  });
  res.json(u_userbiodataok);
});

// ============== create usergameHistories ==============

route.post("/gamehistory", async (req, res) => {
  const { body } = req;
  const history = await usergameHistory.create(body);
  res.json(history);
});

route.get("/gamehistory", async (req, res) => {
  const cek_historyOK = await usergameHistory.findAll();
  res.json(cek_historyOK);
});

route.put("/gamehistory/:id", async (req, res) => {
  const { body } = req;
  const u_cekidhistoryOK = await usergameHistory.update(body, {
    where: {
      id: req.params.id,
    },
  });
  res.json(u_cekidhistoryOK);
});

route.delete("/gamehistory/:id", async (req, res) => {
  const u_cekhapusIdhistory = await usergameHistory.destroy({
    where: {
      id: req.params.id,
    },
  });
  res.json(u_cekhapusIdhistory);
});

module.exports = route;
