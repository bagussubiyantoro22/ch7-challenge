"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    return queryInterface.bulkInsert("userGames", [
      {
        id: "1",
        email: "xbegee@gmail.com",
        username: "xbege",
        password: "xbege",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "2",
        email: "xbegee1@gmail.com",
        username: "xbege1",
        password: "xbege1",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "3",
        email: "xbegee2@gmail.com",
        username: "xbege2",
        password: "xbege2",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  async down(queryInterface, Sequelize) {
    return queryInterface.bulkDelete("userGames", null, {});
  },
};
