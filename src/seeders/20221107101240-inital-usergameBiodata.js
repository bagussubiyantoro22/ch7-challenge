"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    return queryInterface.bulkInsert("usergameBiodata", [
      {
        id: "1",
        name: "bege1",
        job: "warrior",
        status: "newbie",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "2",
        name: "bege2",
        job: "warrior",
        status: "newbie",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "3",
        name: "bege3",
        job: "warrior",
        status: "newbie",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  async down(queryInterface, Sequelize) {
    return queryInterface.bulkDelete("usergameBiodata", null, {});
  },
};
