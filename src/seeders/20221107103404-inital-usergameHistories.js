"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    return queryInterface.bulkInsert("usergameHistories", [
      {
        id: "1",
        job: "warrior",
        player: "bege",
        result: "win",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "2",
        job: "sage",
        player: "xbege",
        result: "draw",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "3",
        job: "duelist",
        player: "xbegee",
        result: "lose",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  async down(queryInterface, Sequelize) {
    return queryInterface.bulkDelete("usergameHistories", null, {});
  },
};
